FROM openjdk:8-jre

ENV WIREMOCK_VERSION 2.26.0

# default directory
RUN mkdir /opt/wiremock
WORKDIR /opt/wiremock

# download wiremock stand alone build
RUN wget -O wiremock-standalone.jar https://repo1.maven.org/maven2/com/github/tomakehurst/wiremock-jre8-standalone/$WIREMOCK_VERSION/wiremock-jre8-standalone-$WIREMOCK_VERSION.jar    

# expose wiremock ports
EXPOSE 8080 8443

# start wiremock
CMD java -jar wiremock-standalone.jar --verbose --global-response-templating

# ENTRYPOINT ["java","-jar","wiremock-standalone.jar","--verbose", "--global-response-templating", "--port", "80"]