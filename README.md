# Sabre Mock Server
The mock server is runing on [WireMock](http://wiremock.org)

## Setup Locally

To setup  the Sabre mock server it is needed to clone this repo.


### With Docker
Runing with docker avoids to install Java. The container comes with whatever is needed.


Build for the First time
```bash
docker-compose up --build wiremock
```

Run Container (after it was build)
```bash
docker-compose up wiremock

Successfully tagged 
Recreating wiremock-standalone ... done
Attaching to wiremock-standalone
wiremock-standalone | 2020-02-19 00:41:03.009 Verbose logging enabled
wiremock-standalone | SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
wiremock-standalone | SLF4J: Defaulting to no-operation (NOP) logger implementation
wiremock-standalone | SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
wiremock-standalone | 2020-02-19 00:41:03.977 Verbose logging enabled
wiremock-standalone |  /$$      /$$ /$$                     /$$      /$$                     /$$      
wiremock-standalone | | $$  /$ | $$|__/                    | $$$    /$$$                    | $$      
wiremock-standalone | | $$ /$$$| $$ /$$  /$$$$$$   /$$$$$$ | $$$$  /$$$$  /$$$$$$   /$$$$$$$| $$   /$$
wiremock-standalone | | $$/$$ $$ $$| $$ /$$__  $$ /$$__  $$| $$ $$/$$ $$ /$$__  $$ /$$_____/| $$  /$$/
wiremock-standalone | | $$$$_  $$$$| $$| $$  \__/| $$$$$$$$| $$  $$$| $$| $$  \ $$| $$      | $$$$$$/ 
wiremock-standalone | | $$$/ \  $$$| $$| $$      | $$_____/| $$\  $ | $$| $$  | $$| $$      | $$_  $$ 
wiremock-standalone | | $$/   \  $$| $$| $$      |  $$$$$$$| $$ \/  | $$|  $$$$$$/|  $$$$$$$| $$ \  $$
wiremock-standalone | |__/     \__/|__/|__/       \_______/|__/     |__/ \______/  \_______/|__/  \__/
wiremock-standalone | 
wiremock-standalone | port:                         8080
wiremock-standalone | enable-browser-proxying:      false
wiremock-standalone | disable-banner:               false
wiremock-standalone | no-request-journal:           false
wiremock-standalone | verbose:                      true
wiremock-standalone | 
```


### Without Docker
If docker is not setup, then it can be run using Java.

```bash
java -jar wiremock-standalone-x.xx.x.jar --verbose --global-response-templating
```


## Wiremock Tips

### Run in Recording Mode
This will monitor any call to the real service and will record all request/response. 

```bash
java -jar wiremock-standalone-x.xx.x.jar --proxy-all="https://webservices.havail.sabre.com" --record-mappings --verbose
```
	

## Testting
Go to http://localhost:8080




## References
- [popularowl.com](https://www.popularowl.com/api-first/automate-api-mocking-with-wiremock-and-terraform/)
- [rodolpheche docker image](https://github.com/rodolpheche/wiremock-docker)
- [gerardorf docker image](https://github.com/gerardorf/wiremock-docker/blob/master/Dockerfile)